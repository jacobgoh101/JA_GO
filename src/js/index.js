import "./polyfills";
import "./includes/ie11Handler";
import "./includes/navGradientHandler";

import desktopEditHandler from './includes/desktopEditHandler';
desktopEditHandler();
import mobileEditHandler from './includes/mobileEditHandler';
mobileEditHandler();

// profile header top nav tooltip handler
import "./includes/profileHeaderTopNavTooltip";

// profile header nav handler
// including function for showing 'more' icon on window resize
import "./includes/profileHeaderNavHandler";