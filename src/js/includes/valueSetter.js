// for synchonizing desktop mobile fields
export default function({
    value,
    type
}) {
    const screens = ['desktop', 'mobile'];
    screens.map(s => {
        const elem = document.querySelector(`#input-value-${s}-${type}`);
        if (elem)
            elem.textContent = value;
    });
}