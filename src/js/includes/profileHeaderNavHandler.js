// utility
const forEach = function(array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]); // passes back stuff we need
    }
};

const navMoreHandler = () => {
    const isMobile = window.innerWidth < 768;
    if (isMobile) return;

    const navLeftPadding = 185;
    const navRightPadding = 160;
    const navContainer = document.querySelector(".profile-header__nav");
    const navItems = document.querySelectorAll(".profile-header__nav-item.profile-header__nav-item--visible");
    const allNavItems = document.querySelectorAll(".profile-header__nav-item");
    const hiddenItemsExist = navItems.length !== allNavItems.length;
    const lastNavItem = navItems[navItems.length - 1];
    const dropdownButton = document.querySelector(".profile-header__nav-more");
    const dropdownContainer = document.querySelector(".profile-header__nav-more-dropdown");

    const navContainerWidth = navContainer.offsetWidth - navLeftPadding - navRightPadding;

    let navItemsWidth = [];
    forEach(navItems, function(index, elem) {
        navItemsWidth.push(elem.offsetWidth);
    });
    let navItemsWidthTotal = 0;
    if (navItemsWidth.length)
        navItemsWidthTotal = navItemsWidth.reduce(function(acc, val) {
            return acc + val;
        });

    const overflowing = navItemsWidthTotal > navContainerWidth;

    if (overflowing) {
        const linkElem = lastNavItem.querySelector('a');
        const linkHref = linkElem.href;
        const linkText = linkElem.innerText;
        lastNavItem
            .classList
            .remove('profile-header__nav-item--visible');

        // create dropdown link elem
        const dropdownLinkElem = document.createElement('a');
        dropdownLinkElem.href = linkHref;
        dropdownLinkElem.innerText = linkText;
        dropdownLinkElem
            .classList
            .add('profile-header__nav-more-dropdown__item');
        dropdownContainer.appendChild(dropdownLinkElem, null);
        dropdownButton
            .classList
            .remove('hidden');
        return navMoreHandler();
    } else {
        const firstInvisibleNavItem = document.querySelectorAll(".profile-header__nav-item:not(.profile-header__nav-item--visible)")[0];
        const firstInvisibleNavItemWidth = firstInvisibleNavItem ?
            firstInvisibleNavItem.offsetWidth :
            null;
        const shouldAddBackFirstInvisibleNavItem = firstInvisibleNavItemWidth ?
            !(navItemsWidthTotal + firstInvisibleNavItemWidth > navContainerWidth) :
            false;

        if (hiddenItemsExist && shouldAddBackFirstInvisibleNavItem) {
            const dropdownLinkElems = document.querySelectorAll('.profile-header__nav-more-dropdown__item');
            if (!dropdownLinkElems.length)
                return;
            const lastDropdownLinkElem = dropdownLinkElems[dropdownLinkElems.length - 1];
            lastDropdownLinkElem.remove();

            firstInvisibleNavItem
                .classList
                .add('profile-header__nav-item--visible');

            return navMoreHandler();
        } else if (!hiddenItemsExist) {
            dropdownButton
                .classList
                .add('hidden');
        }
    }

    if (!hiddenItemsExist)
        dropdownButton.classList.add('hidden');
};
navMoreHandler();

window.addEventListener('resize', () => {
    navMoreHandler();
});