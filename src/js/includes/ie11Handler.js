window.isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

if (isIE11) {
    document.body.className += ' ' + 'ie11';
}