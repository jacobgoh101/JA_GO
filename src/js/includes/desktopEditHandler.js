import {
    InputTooltip
} from "./InputTooltip";

// desktop edit button
export default function() {
    window.currentInputTooltip = null;
    document.querySelectorAll('.tab-content-about__info-btn-edit')
        .forEach(elem => elem.addEventListener('click', (e) => {
            const el = e.currentTarget.dataset.target;
            const type = e.currentTarget.dataset.type;
            let label = e.currentTarget.dataset.label;
            if (label.indexOf('[') > -1) label = JSON.parse(label);
            if (window.currentInputTooltip !== null) window.currentInputTooltip.destroy();
            window.currentInputTooltip = new InputTooltip({
                el,
                label,
                type
            });
            currentInputTooltip.mount();
        }));
}