import valueSetter from "./valueSetter";

export class InputTooltip {
    constructor({
        el,
        label,
        type
    }) {
        this.label = label;
        this.type = type;
        this.elSelector = el;
        this.el = document.querySelector(el);
        this.value = this.el.textContent.trim();

        // handle first,last name
        if (this.label.indexOf('[') > -1)
            this.label = JSON.parse(this.label);
    }
    mount() {
        // add tooltip
        this
            .el
            .classList
            .add("input-tooltips", "input-tooltips--active");
        this
            .el
            .insertAdjacentHTML('beforeend', `
            <div class="input-tooltips__content">
                ${this.getInputHTML()}
                <div class="input-tooltips__btns">
                    <a class="btn-primary btn-primary--wide input-tooltips__btn-save">
                        SAVE
                    </a>
                    <a class="btn-primary btn-primary--outline btn-primary--wide input-tooltips__btn-cancel">
                        CANCEL
                    </a>
                </div>
            </div>
            `);
        // auto focus
        this.el.querySelector('input').focus();
        // save/cancel event
        this.el.querySelector('.input-tooltips__btn-save').addEventListener('click', (e) => {
            this.save();
        });
        this.el.querySelector('.input-tooltips__btn-cancel').addEventListener('click', (e) => {
            this.cancel();
        });
        // save on 'enter' key
        this.el.querySelectorAll('input')
            .forEach(elem => elem.addEventListener('keypress', (e) => {
                var key = e.which || e.keyCode;
                if (key === 13) this.save();
            }));

        // hide edit button
        document.querySelector(`[data-target="${this.elSelector}"]`).classList.add('hidden');

        // fix ie11 bug, helping it to focus..
        if (isIE11)
            this.el.querySelectorAll('input')
            .forEach(elem => elem.addEventListener('click', (e) => {
                e.target.blur();
                e.target.focus();
            }));
    }
    destroy() {
        // remove tooltip
        if (this.el.querySelector('.input-tooltips__content'))
            this.el.querySelector('.input-tooltips__content').remove();
        this
            .el
            .classList
            .remove("input-tooltips", "input-tooltips--active");

        // unhide edit button
        document.querySelector(`[data-target="${this.elSelector}"]`).classList.remove('hidden');
    }
    save() {
        if (this.isNameField() && !this.validateName()) return;
        if (this.isNameField()) {
            valueSetter({
                value: this.getFirstName(),
                type: 'firstname'
            });
            valueSetter({
                value: this.getLastName(),
                type: 'lastname'
            });
        } else
            valueSetter({
                value: this.getInputValue(),
                type: this.type
            });
        this.destroy();
    }
    cancel() {
        this.destroy();
    }
    getInputHTML() {
        if (this.isNameField()) {
            // handle first,last name
            const names = this.value.split(' ').filter(v => v);
            return this.label.map((l, i) => {
                return `
                <div class="material-input-group material-input-group--tooltips">      
                    <input type="text" class="material-input-group__input" required value="${names[i]}">
                    <span class="material-input-group__highlight"></span>
                    <span class="material-input-group__bar"></span>
                    <label class="material-input-group__label">${l}</label>
                </div>
                `;
            }).join('');
        } else {
            return `
            <div class="material-input-group material-input-group--tooltips">      
                <input type="text" class="material-input-group__input" required value="${this.value}">
                <span class="material-input-group__highlight"></span>
                <span class="material-input-group__bar"></span>
                <label class="material-input-group__label">${this.label}</label>
            </div>
            `;
        }
    }
    isNameField() {
        return Array.isArray(this.label);
    }
    getInputValue() {
        let value = '';
        if (this.isNameField()) {
            this.el.querySelectorAll('input')
                .forEach(elem => {
                    value += elem.value + ' ';
                });
        } else {
            value = this.el.querySelector('input').value;
        }
        return value;
    }
    getFirstName() {
        return this.getInputValue().split(' ')[0];
    }
    getLastName() {
        return this.getInputValue().split(' ')[1];
    }
    validateName() {
        if (this.isNameField()) {
            let validated = true;
            this.el.querySelectorAll('input')
                .forEach(elem => {
                    if (validated && elem.value.trim().split(' ').length > 1) {
                        alert(`Space isn't allowed in name field.`);
                        validated = false;
                    }
                });
            return validated;
        }
        return false;
    }
}