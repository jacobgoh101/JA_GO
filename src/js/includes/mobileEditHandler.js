import valueSetter from "./valueSetter";

export default function() {
    // buttons
    const btnEdit = document.querySelector('.tab-content__mobile-edit-btn-edit');
    const btnCancel = document.querySelector('.tab-content__mobile-edit-btn-cancel');
    const btnSave = document.querySelector('.tab-content__mobile-edit-btn-save');
    if (!(btnEdit && btnCancel && btnSave)) return;

    const inputNames = ['firstname', 'lastname', 'website', 'phone', 'address'];

    // show/hide form
    const actionsWrapperElem = document.querySelector('.tab-content__mobile-edit-actions');
    const formWrapperElem = document.querySelector('.tab-content-about');
    const showForm = () => {
        actionsWrapperElem.classList.remove('tab-content__mobile-edit-actions--default');
        formWrapperElem.classList.remove('tab-content-about--default');

        actionsWrapperElem.classList.add('tab-content__mobile-edit-actions--editing');
        formWrapperElem.classList.add('tab-content-about--editing');
    };
    const hideForm = () => {
        actionsWrapperElem.classList.add('tab-content__mobile-edit-actions--default');
        formWrapperElem.classList.add('tab-content-about--default');

        actionsWrapperElem.classList.remove('tab-content__mobile-edit-actions--editing');
        formWrapperElem.classList.remove('tab-content-about--editing');
    };
    const setFieldValues = () => {
        inputNames.map(name => {
            document.querySelector(`[name="input-value-mobile-${name}"]`).value = document.querySelector(`#input-value-mobile-${name}`).textContent.trim();
        })
    };
    btnEdit.addEventListener('click', (e) => {
        setFieldValues();
        showForm()
    });
    btnCancel.addEventListener('click', (e) => {
        hideForm()
    });

    // saving
    btnSave.addEventListener('click', (e) => {
        inputNames.map(name => {
            const value = document.querySelector(`[name="input-value-mobile-${name}"]`).value.trim();
            valueSetter({
                value,
                type: name
            })
        });
        hideForm();
    });
}